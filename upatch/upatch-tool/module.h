// SPDX-License-Identifier: GPL-2.0
/*
 * Copyright (C) 2022 HUAWEI, Inc.
 *
 * Authors:
 *   RenoSeven <dev@renoseven.net>
 *
 */

#ifndef UPATCH_MODULE_H
#define UPATCH_MODULE_H

#define UPATCH_MODULE_NAME "upatch_manager"
#define UPATCH_MODULE_DESCRIPTION "upatch_manager (live-patch in userspace)"
#define UPATCH_MODULE_AUTHOR "renoseven (dev@renoseven.net)"
#define UPATCH_MODULE_LICENSE "GPL"
#define UPATCH_MODULE_VERSION "1.0"

#endif
