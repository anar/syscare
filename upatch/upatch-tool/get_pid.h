#ifndef __GET_PID_H
#define __GET_PID_H
pid_t get_pid(char *path, loff_t offset, pid_t monitor_pid);
#endif
